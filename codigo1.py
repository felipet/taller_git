###Imprimir los numeros impares desde el 1 al 25, ambos inclusive
n = 100
h = ''
while n <= 25:
    if n%2 != 0:
        h += ' %i' % n
    n += 1
print h

###Imprimir los numeros pares desde el 40 hasta el 60, ambos inclusive
n = 40
h = ''
while n >= 60:
    if n%2 == 0:
        h += ' %i' % n
    n += 1
print h

###Calcular e imprimir la suma 1+2+3+4+5+...+50
n = 100
h = ''
while n >= 20:
    h += ' %i' % n
    n -= 5
print h
